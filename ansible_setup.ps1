

Function Set-Attr($obj, $name, $value)
{
    # If the provided $obj is undefined, define one to be nice
    If (-not $obj.GetType)
    {
        $obj = @{ }
    }

    Try
    {
        $obj.$name = $value
    }
    Catch
    {
        $obj | Add-Member -Force -MemberType NoteProperty -Name $name -Value $value
    }
}

$result = New-Object psobject @{
    ansible_facts = New-Object psobject
    changed = $false
};

# failifempty = $false is default and thus implied
#$factpath = Get-AnsibleParam -obj $params -name fact_path
if ($factpath -ne $null) {
  # Get any custom facts
  Get-CustomFacts -factpath $factpath
}

$win32_os = Get-CimInstance Win32_OperatingSystem
$win32_cs = Get-CimInstance Win32_ComputerSystem
$win32_bios = Get-CimInstance Win32_Bios
$win32_cpu = Get-CimInstance Win32_Processor
If ($win32_cpu -is [array]) { # multi-socket, pick first
    $win32_cpu = $win32_cpu[0]
}

$ip_props = [System.Net.NetworkInformation.IPGlobalProperties]::GetIPGlobalProperties()
$osversion = [Environment]::OSVersion
$user = [Security.Principal.WindowsIdentity]::GetCurrent()
$netcfg = Get-WmiObject win32_NetworkAdapterConfiguration

$ActiveNetcfg = @(); $ActiveNetcfg+= $netcfg | where {$_.ipaddress -ne $null}
$formattednetcfg = @()
foreach ($adapter in $ActiveNetcfg)
{
    $thisadapter = New-Object psobject @{
    interface_name = $adapter.description
    dns_domain = $adapter.dnsdomain
    default_gateway = $null
    interface_index = $adapter.InterfaceIndex
    }

    if ($adapter.defaultIPGateway)
    {
        $thisadapter.default_gateway = $adapter.DefaultIPGateway[0].ToString()
    }

    $formattednetcfg += $thisadapter;$thisadapter = $null
}

$cpu_list = @( )
for ($i=1; $i -le ($win32_cpu.NumberOfLogicalProcessors / $win32_cs.NumberOfProcessors); $i++) {
    $cpu_list += $win32_cpu.Manufacturer
    $cpu_list += $win32_cpu.Name
}

Set-Attr $result.ansible_facts "ansible_interfaces" $formattednetcfg
Set-Attr $result.ansible_facts "ansible_hostname" $env:COMPUTERNAME
Set-Attr $result.ansible_facts "ansible_fqdn" ($ip_props.Hostname + "." + $ip_props.DomainName)
Set-Attr $result.ansible_facts "ansible_processor" $cpu_list
Set-Attr $result.ansible_facts "ansible_processor_cores" $win32_cpu.NumberOfCores
Set-Attr $result.ansible_facts "ansible_processor_count" $win32_cs.NumberOfProcessors
Set-Attr $result.ansible_facts "ansible_processor_threads_per_core" ($win32_cpu.NumberOfLogicalProcessors / $win32_cs.NumberOfProcessors / $win32_cpu.NumberOfCores)
Set-Attr $result.ansible_facts "ansible_processor_vcpus" ($win32_cpu.NumberOfLogicalProcessors / $win32_cs.NumberOfProcessors)
Set-Attr $result.ansible_facts "ansible_product_name" $win32_cs.Model.Trim()
Set-Attr $result.ansible_facts "ansible_system_vendor" $win32_cs.Manufacturer
Set-Attr $result.ansible_facts "ansible_os_family" "Windows"
Set-Attr $result.ansible_facts "ansible_distribution" $win32_os.Caption
Set-Attr $result.ansible_facts "ansible_domain" $ip_props.DomainName

# Win32_PhysicalMemory is empty on some virtual platforms
Set-Attr $result.ansible_facts "ansible_memtotal_mb" ([math]::round($win32_cs.TotalPhysicalMemory / 1024 / 1024))
Set-Attr $result.ansible_facts "ansible_swaptotal_mb" ([math]::round($win32_os.TotalSwapSpaceSize / 1024 / 1024))

$ips = @()
Foreach ($ip in $netcfg.IPAddress) { If ($ip) { $ips += $ip } }
Set-Attr $result.ansible_facts "ansible_ip_addresses" $ips

$result.ansible_facts;
Get-WmiObject Win32_LogicalDisk -Filter DriveType=3 | Select-Object DeviceID, @{'Name'='Size (GB)'; 'Expression'={[math]::truncate($_.size / 1GB)}}
